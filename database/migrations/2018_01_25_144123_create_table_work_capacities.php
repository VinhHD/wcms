<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWorkCapacities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_capacities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('startDate');
            $table->date('endDate');
            $table->string('available_capacity')->nullable();
            $table->string('reason_available_capacity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_capacities');
    }
}
