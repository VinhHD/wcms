<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('note_name')->after('name')->nullable();
            $table->date('admission_date')->before('oid')->nullable();
            $table->string('admission_no')->after('oid')->nullable();
            $table->string('fax_number')->after('extn')->nullable();
            $table->string('cluster')->after('fax_number')->nullable();
            $table->string('cluster_head')->after('cluster')->nullable();
            $table->string('available_capacity')->after('fax_number')->nullable();
            $table->string('reason_available_capacity')->after('available_capacity')->nullable();
            $table->string('send_email')->after('reason_available_capacity')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
