<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;
    $availability = ['lessoneday', 'uponeday', 'dayweek', '2week', 'nooneweek', 'notwoweek', 'onleave', 'other'];
    return [
        'name' => $faker->name,
        'note_name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'employee_code' => strtoupper(str_random(4)),
        'department' => 'Department',
        'designation' => 'Designation',
        'extn' => $faker->phoneNumber,
        'fax_number' => $faker->phoneNumber,
        'office' => '5th Floor',
        'available_capacity' => $availability[rand(0,7)],
        'admission_date' => $faker->date(),
        'admission_no' => '123/2015',
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
