$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
   $(".file-office .office-type").click(function () {
       $(".file-reference").show(200, function () {
           $("#file-reference-no").text("999999.99999");
           $("#file-reference-input").val("999999.99999");
       });

       $(".fileref-info").hide(200, function () {
           $("#client_name").text('');
           $("#matter_name").text('');

           $("#input-client_name").val('');
           $("#input-matter_name").val('');
       });
   });
});