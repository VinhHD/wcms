<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');
Route::group(['prefix' => 'cronjobs'], function(){
    Route::get('/reminder', 'CronJobsController@reminder');
});
Route::group(['namespace' => 'Auth'], function()
{
    Route::get('/', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('import-users', 'UsersController@import_user')->name('import_users');
});

Route::middleware(['auth'])->group(function () {
    Route::group(['namespace' => 'Lawyers', 'middleware'=>'lawyer'], function()
    {
        Route::get('check-in', 'WeeklyWorkflowController@checkUp')->name('workflow.checkUp');
        Route::post('check-in/store', 'WeeklyWorkflowController@store')->name('workflow.checkUp.store');
    });

    Route::group(['namespace' => 'Admin', 'middleware'=>'clusterHead'], function()
    {
        Route::get('cluster-head', 'CheckUpController@index')->name('admin.checkUp');
        Route::post('work-assignment', 'CheckUpController@postWorkAssignment')->name('admin.send.assignment');
        Route::post('update-capacity', 'CheckUpController@store')->name('admin.update.capacity');
    });
    Route::group(['namespace' => 'Auth'], function()
    {
        Route::get('select2-search-user', 'UsersController@searchByTitle')->name('user_by_name');
    });
});