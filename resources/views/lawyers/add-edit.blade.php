@extends('layout.base')

@section('page_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('css/select2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/add_edit.css')}}"/>
    <style type="text/css">
        .name-on-card .search-name-input {
            margin-top: -33px;
        }

        .name-on-card .select2-container {
            visibility: hidden;
        }
    </style>
@endsection

@section('content')

    <div class="container">

        <section class="form-header">
            <table class="table">
                <thead>
                    <tr>
                        <th>{{env('APP_NAME', 'Stationery Requisition')}}</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Requested By:</td>
                        <td>{{isset($my_request) ? $my_request->user()->first()->name : user('name')}}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Requested On:</td>
                        <td>{{isset($my_request) ? dateFormat($my_request->date) : currentTime()}}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </section>

        <div style="height: 10px"></div>

        @if(session('submit_error'))
            <section class="form-error">
                <div class="alert alert-danger">
                    <p>{{session('submit_error')}}</p>
                </div>
            </section>
        @endif

        <section class="form-main">

            <form id="add-edit-form" class="form-horizontal" role="form" method="post" action="">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group {{$errors->has('name_on_card') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="name_on_card">Name to appear on card:</label>
                        </div>
                        <div class="col-md-9 col-sm-9 name-on-card">
                            <select id="name_on_card" class="form-control"></select>
                            <div class="input-group search-name-input">
                                <input class="form-control" name="name_on_card" type="text" value="{{isset($my_request) && sizeof(old()) == 0 ? $my_request->name_on_card : (sizeof(old()) > 0 ? old('name_on_card') : user('name'))}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary search-name-button" type="button" title="Select Name"><i class="fa fa-caret-down" aria-hidden="true"></i></button>
                                </span>
                            </div>
                            <small class="text-danger">{{$errors->first('name_on_card')}}</small>
                        </div>
                    </div>
                </div>

                {{--<div class="row">--}}
                    {{--<div class="text-left">--}}
                        {{--<p style="color: red;">*Free of text up to 36 characters.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="row">
                    <div class="form-group {{$errors->has('designation') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="designation">Designation:</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input id="designation" class="form-control" name="designation" type="text" value="{{isset($my_request) && sizeof(old()) == 0 ? $my_request->designation : (sizeof(old()) > 0 ? old('designation') : user('designation'))}}">
                            <small class="text-danger">{{$errors->first('designation')}}</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group {{$errors->has('email_address') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="email_address">Email Address:</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input id="email_address" class="form-control" name="email_address" type="email" value="{{isset($my_request) && sizeof(old()) == 0 ? $my_request->email_address : (sizeof(old()) > 0 ? old('email_address') : user('email'))}}">
                            <small class="text-danger">{{$errors->first('email_address')}}</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group {{$errors->has('direct_number') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="direct_number">Direct Number:</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input id="direct_number" class="form-control" name="direct_number" type="tel" value="{{isset($my_request) && sizeof(old()) == 0 ? $my_request->direct_number : (sizeof(old()) > 0 ? old('direct_number') : user('extn'))}}">
                            <small class="text-danger">{{$errors->first('direct_number')}}</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group {{$errors->has('fax_number') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="fax_number">Fax Number:</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input id="fax_number" class="form-control" name="fax_number" type="tel" value="{{isset($my_request) && sizeof(old()) == 0 ? $my_request->fax_number : (sizeof(old()) > 0 ? old('fax_number') : user('fax_number'))}}">
                            <small class="text-danger">{{$errors->first('fax_number')}}</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group {{$errors->has('mobile_number') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="mobile_number">Mobile Phone Number:</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input id="mobile_number" class="form-control" name="mobile_number" type="tel" value="{{isset($my_request) && sizeof(old()) == 0 ? $my_request->mobile_number :  old('mobile_number')}}">
                            <small class="text-muted">Put "–"  if you don’t want mobile phone to display on the name card.</small><br>
                            <small class="text-danger">{{$errors->first('mobile_number')}}</small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group {{$errors->has('colour') ? 'has-error' : ''}}">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="colour">Colour:</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <select id="colour" class="form-control" name="colour">
                                <option {{(isset($my_request) && $my_request->colour == 'Black' && sizeof(old()) == 0) || old('colour') == 'Black' ? 'selected' :  ''}} value="Black">Black</option>
                                {{--<option {{(isset($my_request) && $my_request->colour == 'White' && sizeof(old()) == 0) || old('colour') == 'White' ? 'selected' :  ''}} value="White">White</option>--}}
                            </select>
                            <small class="text-danger">{{$errors->first('colour')}}</small>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="text-left">
                        <p style="color: red">*Optional.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-3 col-sm-3">
                            <label class="control-label" for="remark">Remark(s):</label>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <textarea id="remark" class="form-control" rows="3" name="remark">{{isset($my_request) && sizeof(old()) == 0 ? $my_request->remark :  old('remark')}}</textarea>
                        </div>
                    </div>
                </div>

                <div style="height: 20px"></div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <img style="border: 2px solid darkgrey" class="sample-card" src="{{asset('images/namecard.jpg')}}">
                    </div>
                </div>

                <div style="height: 20px"></div>

                <div class="form-buttons">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            @if (isset($my_request))
                                <a class="btn btn-default" href="{{url()->previous()}}">Cancel</a>
                                <button type="submit" class="btn btn-success" name="submit" value="save_edit">Save</button>
                            @else
                                <button type="submit" class="btn btn-success" name="submit" value="submit_for_approval">Submit</button>
                            @endif
                        </div>
                    </div>
                </div>

            </form>
        </section>
    </div>

@endsection

@section('page_scripts')
    <script type="text/javascript" src="{{asset('js/jquery.dirtyforms.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/select2.full.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#name_on_card")
                .select2({
                    placeholder: '',
                    dataType: 'json',
                    ajax: {
                        url: "{{route('user_by_name')}}",
                        delay: 300,
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        _id: item.id,
                                        id: item.name,
                                        text: item.name,
                                        designation: item.designation,
                                        email_address: item.email,
                                        direct_number: item.extn,
                                        fax_number: item.fax_number
                                    }
                                })
                            };
                        },
                        cache: false
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 0,
                    tags: true
                })
                .on('select2:select', function (e) {
                    var data = e.params.data;
                    $("input[name=designation]").val(data.designation);
                    $("input[name=email_address]").val(data.email_address);
                    $("input[name=direct_number]").val(data.direct_number);
                    $("input[name=fax_number]").val(data.fax_number);
                    $("input[name=name_on_card]").val(data.text);
                });

            $(".form-buttons button").click(function() {
                $(".loader").show();
            });

            $(".search-name-button").click(function () {
                $("#name_on_card").select2("open");
            });
        });
    </script>
@endsection