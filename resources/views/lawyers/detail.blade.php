@extends('layout.base')

@section('page_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('css/add_edit.css')}}"/>
@endsection

@section('content')

    <div class="container">

        <section class="form-header">
            <table class="table">
                <thead>
                <tr>
                    <th>{{env('APP_NAME', 'Stationery Requisition')}}</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Requested By:</td>
                    <td>{{$my_request->user->name}}</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Requested On:</td>
                    <td>{{dateFormat($my_request->date)}}</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </section>

        <div style="height: 10px"></div>

        <section class="">

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <label for="name_on_card">Name to appear on card:</label>
                </div>
                <div class="col-md-9 col-sm-9">
                    {{$my_request->name_on_card}}
                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label for="designation">Designation:</label>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        {{$my_request->designation}}
                    </div>

                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-3 col-sm-3">
                        <label for="email_address">Email Address:</label>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        {{$my_request->email_address}}
                    </div>
                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <label for="direct_number">Direct Number:</label>
                </div>
                <div class="col-md-9 col-sm-9">
                    {{$my_request->direct_number}}
                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <label for="fax_number">Fax Number:</label>
                </div>
                <div class="col-md-9 col-sm-9">
                    {{$my_request->fax_number}}
                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <label for="mobile_number">Mobile Phone Number:</label>
                </div>
                <div class="col-md-9 col-sm-9">
                    {{$my_request->mobile_number}}
                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <label for="colour">Colour:</label>
                </div>
                <div class="col-md-9 col-sm-9">
                    {{$my_request->colour}}
                </div>
            </div>

            <div style="height: 10px"></div>

            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <label for="remark">Remark(s):</label>
                </div>
                <div class="col-md-9 col-sm-9">
                    {{$my_request->remark}}
                </div>
            </div>

            <div style="height: 20px"></div>

            <div class="row">
                <div class="col-md-12 text-center">
                    <img style="border: 2px solid darkgrey" class="sample-card" src="{{asset('images/namecard.jpg')}}">
                </div>
            </div>


            <div style="height: 20px"></div>
            <div class="row">
                <div class="col-md-12 text-center button-group">
                    @if ($my_request->status == 'awaiting_approval' && $my_request->user_id == user('id'))
                        <a class="btn btn-default" href="{{route('my_requisitions')}}">Close</a>
                        <a class="btn btn-primary" href="{{route('edit_requisition', ['id' => $my_request->id])}}">Edit</a>
                    @endif

                    @if ($my_request->status == 'awaiting_approval' && isAdmin())
                        @if ($my_request->user_id != user('id'))
                            <a class="btn btn-primary" href="{{route('edit_requisition', ['id' => $my_request->id])}}">Edit</a>
                        @endif
                        <button class="btn btn-warning update-status-button" data-action="denied">Deny</button>
                        <button class="btn btn-success update-status-button" data-action="approved">Approve</button>
                    @endif

                    @if ($my_request->status == 'approved' && isAdmin())
                        <button class="btn btn-warning update-status-button" data-action="ready_collection">Ready for Collection</button>
                    @endif
                </div>
            </div>

        </section>
    </div>

@endsection

@section('page_scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".update-status-button").click(function () {
                var url = '{{route('admin_update_status')}}';
                var data = {
                    action: $(this).data('action'),
                    requests: [{{$my_request->id}}]
                };

                $('.loader').show();
                $.post(url, data, function (data, status) {
                    var response = JSON.parse(data);
                    if (response.status == 'success') {
                        window.location.reload();
                    } else {
                        $('.loader').hide();
                        console.log(response.message);
                    }
                });
            });
        });
    </script>
@endsection