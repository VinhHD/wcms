<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Rajah & Tann Weekly Workflow</title>
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" />
	<link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('css/datatables.min.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" />
	<link type="text/css" rel="stylesheet" href="{{asset('css/check-up.css')}}" />
</head>

<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4">
				<div class="left">
					<div class="left-information">
						<a href="{{url('/')}}">
							<img width="250" src="{{asset('images/rt-logo-white.png')}}">
						</a>
						<h1>Fortnightly </br> Workflow</h1>
						<span>Please be reminded that you are required to submit this form by 6pm today</span>
					</div>
					<div class="footer-left">
						<h4>Welcome, {{Auth::user()->name}}</h4>
						<a class="logout" href="{{route('logout')}}">
							<i class="fa fa-sign-out" aria-hidden="true"></i>
							Logout
						</a>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="right">
					<div class="title">
						<h3>Available Capacity</h3>
						<span>Please choose one of the following that best describe your availability.</span>
					</div>
					@if (session('status'))
						<div class="alert alert-success" style="margin-top:20px">
							You have just successfully updated your availability. Thank you for your submission.
						</div>
					@endif
					@if ($errors->any())
						<div class="alert alert-danger" style="margin-top:20px">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form class="check-up" method="post" action="{{route('workflow.checkUp.store')}}">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-lg-3">
								<label class="items" for="uponeday">
									<input type="radio" name="avaible_capacity" id="uponeday" value="uponeday"/>
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/uponeday.png')}}">
									<span>Available for up to one day</span>
								</label>
							</div>
							<div class="col-lg-3">
								<label class="items" for="dayweek">
									<input type="radio" name="avaible_capacity" id="dayweek" value="dayweek" />
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/day-week.png')}}">
									<span>Available for more than one day, less than a week</span>
								</label>
							</div>
							<div class="col-lg-3">
								<label class="items" for="1stweek">
									<input type="radio" name="avaible_capacity" id="1stweek" value="1stweek"/>
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/day-week.png')}}">
									<span>Available for one week (in the 1st week)</span>
								</label>
							</div>
							<div class="col-lg-3">
								<label class="items" for="2ndweek">
									<input type="radio" name="avaible_capacity" id="2ndweek" value="2ndweek" />
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/day-week.png')}}">
									<span>Available for one week (in the 2nd week)</span>
								</label>
							</div>
							<div class="col-lg-3">
								<label class="items" for="2week">
									<input type="radio" name="avaible_capacity" id="2week" value="2week"/>
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/2week.png')}}">
									<span>Available for two week </span>
								</label>
							</div>
							<div class="col-lg-3">
								<label class="items" for="notwoweek">
									<input type="radio" name="avaible_capacity" id="notwoweek" value="notwoweek"/>
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/notwoweek.png')}}">
									<span>No available capacity for two weeks</span>
								</label>
							</div>
							<div class="col-lg-3">
								<label class="items" for="onleave">
									<input type="radio" name="avaible_capacity" id="onleave" value="onleave"/>
									<div class="dra-radio"></div>
									<img width="100" src="{{asset('images/leave.png')}}">
									<span>On leave / Out-of-office</span>
								</label>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label class="other-reason">
									<div class="box-radio">
										<input {{ old('avaible_capacity') == 'other' ? 'checked' : null }} type="radio" name="avaible_capacity" id="other" value="other"/>
										<div class="dra-radio"></div>
									</div>
									<div>If others, please specify your reason(s) below</div>
								</label>
								<textarea name="other_reason">{{old('other_reason')}}</textarea>
								<input class="dra-btn" type="submit" value="Submit" />
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<footer>© 2018 Rajah & Tann Asia. All Rights Reserved.</footer>
			</div>
		</div>

	</div>
</body>