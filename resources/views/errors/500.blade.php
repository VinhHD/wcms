@extends('layout.base')

@section('title')
    @parent
    - 500
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="huge">500</h1>
                <hr class="sm">
                <p><strong>Whoops, looks like something went wrong.</strong></p>
                <p>Click <a style="color: #01aff0; font-weight: bold" href="{{url()->previous()}}">here</a> to go back!</p>
            </div>
        </div>
    </div>
@endsection