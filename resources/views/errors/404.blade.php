@extends('layout.base')

@section('title')
    @parent
    - 404
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="huge">404</h1>
                <hr class="sm">
                <p><strong>Sorry, the page you are looking for could not be found.</strong></p>
                <p>The page you are looking for was moved, removed, renamed or might never existed.</p>
            </div>
        </div>
    </div>
@endsection