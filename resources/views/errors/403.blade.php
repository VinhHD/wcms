@extends('layout.base')

@section('title')
    @parent
    - 403
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="huge">403</h1>
                <hr class="sm">
                <p><strong>Forbidden</strong></p>
                <p>You don't have permission to access to this page.</p>
            </div>
        </div>
    </div>
@endsection