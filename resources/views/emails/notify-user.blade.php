@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        {{ config('app.name') }}
    @endcomponent
@endslot
<p>Name: {{$requisition->user->name}}</p>
<p>Department: {{$requisition->user->department}}</p>
<p>Designation: {{$requisition->user->designation}}</p>
<p>Requested On: {{dateFormat($requisition->date)}}</p>
<table border="1" width='900' cellpadding='5' cellspacing="0" style='border-collapse: collapse;font-size:12px'>
    <thead>
        <tr>
            <th>Name to appear on card</th>
            <th>Designation</th>
            <th>Email Address</th>
            <th>Direct Number</th>
            <th>Fax Number</th>
            <th>Mobile Phone Number</th>
            <th>Colour</th>
        </tr>
    </thead>
    <tbody>
        <tr id="row-{{$loop->iteration}}">
            <td>{{$requisition->name_on_card}}</td>
            <td width="180">{{$requisition->designation}}</td>
            <td>{{$requisition->email_address}}</td>
            <td>{{$requisition->direct_number}}</td>
            <td>{{$requisition->fax_number}}</td>
            <td>{{$requisition->mobile_number}}</td>
            <td>{{$requisition->colour}}</td>
        </tr>
    </tbody>
</table>
<br/>
<p>Remark(s): {{$requisition->remark}}</p>
@endcomponent
