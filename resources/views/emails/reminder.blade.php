@component('mail::message')
<p>Dear, <b>{{$user->name}}</b></p>
<p>Please submit your response to the WCMS by 6:00 pm today. You can access the WCMS by clicking on the link below to submit your response.</p>
<p><a>{{route('workflow.checkUp')}}</a></p>
@endcomponent
