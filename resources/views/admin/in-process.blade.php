@extends('layout.base')

@section('title')
    @parent
    - {{$page_title}}
@endsection

@section('page_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('css/buttons.dataTables.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/select.dataTables.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/rowGroup.dataTables.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/left_sidebar.css')}}"/>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('layout.left_side')
            </div>

            <div class="col-md-9">
                <h4 class="page-title">{{$page_title}}</h4>

                <hr class="divider"/>

                <table id="list-requests" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Direct Number</th>
                        <th>Fax Number</th>
                        <th>Mobile Phone Number</th>
                        <th>Colour</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page_scripts')
    <script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.select.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.rowGroup.min.js')}}"></script>
    <script type="text/javascript">
        var RequestsTable = function() {
            this.requests_table = $('#list-requests');

            this.init();
            this.addListener();
        };

        RequestsTable.prototype = {
            init : function() {
                this.requests_table = this.requests_table.DataTable({
                    pageLength: 25,
                    searching: false,
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    dom: "<'row'<'col-sm-9'B><'col-sm-3'l>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    ajax: {
                        url: '{{route('ajax_admin_approved')}}',
                        type: "POST"
                    },
                    select: {
                        style: 'multi',
                        selector: 'td:first-child'
                    },
                    columns: [
                        {
                            "class": "select-checkbox",
                            orderable: false,
                            data: null,
                            defaultContent: ""
                        },
                        {
                            data: 'date',
                            name: 'date',
                            render: function ( data, type, row ) {
                                return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY");
                            }
                        },
                        {
                            data: 'name_on_card',
                            name: 'name_on_card'
                        },
                        {
                            data: 'designation',
                            name: 'designation'
                        },
                        {
                            data: 'direct_number',
                            name: 'direct_number'
                        },
                        {
                            data: 'fax_number',
                            name: 'fax_number'
                        },
                        {
                            data: 'mobile_number',
                            name: 'mobile_number'
                        },
                        {
                            data: 'colour',
                            name: 'colour'
                        },
                        {
                            data: 'status',
                            name: 'status',
                            render: function ( data, type, row ) {
                                var label = '';

                                switch (data) {
                                    case 'awaiting_approval':
                                        label = 'Awaiting Approval';
                                        break;
                                    case 'approved':
                                        label = 'In Progress';
                                        break;
                                    case 'ready_collection':
                                        label = 'Ready for Collection';
                                        break;
                                    default:
                                        label = data.charAt(0).toUpperCase() + data.slice(1);
                                        break;
                                }

                                return label;
                            }
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    rowGroup: {
                        dataSrc: 'date',
                        startRender: function ( rows, group ) {
                            return $('<tr/>')
                                .append('<td colspan="10"> ' + moment(group, "YYYY-MM-DD").format("DD/MM/YYYY") + '</td>');
                        }
                    },
                    buttons: [
                        {
                            extend: 'selectAll',
                            className: 'btn btn-default custom-button'
                        },
                        {
                            extend: 'selectNone',
                            className: 'btn btn-default custom-button'
                        },
                        {
                            text: 'Ready for Collection',
                            className: 'btn btn-default custom-button',
                            action: function (e, dt, node, config) {
                                var requests_id = [];
                                dt.rows({selected: true}).every(function (rowIdx, tableLoop, rowLoop) {
                                    var data = this.data();
                                    requests_id.push(data.id);
                                });

                                var url = '{{route('admin_update_status')}}';
                                var data = {
                                    action: 'ready_collection',
                                    requests: requests_id
                                };

                                $('.loader').show();
                                $.post(url, data, function (data, status) {
                                    var response = JSON.parse(data);
                                    if (response.status == 'success') {
                                        window.location.reload();
                                    } else {
                                        $('.loader').hide();
                                        console.log(response.message);
                                    }
                                });
                            },
                            enabled: false
                        }
                    ],
                    initComplete: function (settings, json) {
                        $(".custom-button").removeClass("dt-button");
                    }
                });
            },

            addListener: function () {
                var self = this;
                self.requests_table
                    .on('select', function () {
                        var selectedRows = self.requests_table.rows({selected: true}).count();
                        self.requests_table.button([2]).enable(selectedRows !== 0);
                    })
                    .on('deselect', function () {
                        var selectedRows = self.requests_table.rows({selected: true}).count();
                        self.requests_table.button([2]).enable(selectedRows !== 0);
                    });
            }
        };

        $(document).ready( function () {
            new RequestsTable();
        });

    </script>
@endsection