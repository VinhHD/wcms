@extends('layout.base')
@section('title')
    @parent {{$page_title}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="title">
                    <h3>Summary</h3>
                    <h4 style="color:#444">Fortnightly Work Capacity - {{$schedule}}</h4>
                </div>
                <div class="filter row">
                    <form method="get">
                        <div class="start-date col-lg-2">
                            <div class="form-group">
                                <label>Start Date</label>
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' value="{{$startDate->format('Y-m-d')}}" name="start_date" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="end-date col-lg-2">
                            <div class="form-group">
                                <label>End Date</label>
                                <div class='input-group date' >
                                    <input disabled type='text' value="{{$endDate->format('Y-m-d')}}" name="end_date" class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <label style="visibility: hidden">Search</label>
                            <input style="width:100%; background: #de6737;border-color: #de6737;display: block" type="submit" value="Search" class="btn btn-primary"/>
                        </div>
                        <div class="col-lg-1" >
                            <label style="visibility: hidden">Search</label>
                            <a href="{{route('admin.checkUp')}}">
                                <input style="width:100%; background: #444444;border-color: #444444;display: block" type="button" value="Refresh" class="btn btn-primary"/>
                            </a>
                        </div>
                    </form>
                </div>
                <div class="note">
                    <div class="node-item">
                        <span class="box busy"></span>
                        <span>Busy</span>
                    </div>
                    <div class="node-item">
                        <span class="box available"></span>
                        <span>Available</span>
                    </div>
                </div>

                <table class="checkup-list">
                    <tr>
                        <th>Lawyers</th>
                        <th colspan="5" style="text-align: center;border-bottom: 1px solid #eee;">Available capacity of</th>
                        <th>No available capacity for two weeks</th>
                        <th>On leave/Out-of-office</th>
                        <th>Others (Reason)</th>
                        @if(Auth::user()->email == 'simon.goh@rajahtann.com'
                            || Auth::user()->email == 'wendy.lee@rajahtann.com'
                            || Auth::user()->email == 'ba.sou.ong@rajahtann.com')
                        <th></th>
                        @endif
                    </tr>
                    <tr>
                        <th></th>
                        <th>Up to one day</th>
                        <th>More than one day, less than one week</th>
                        <th>One week (in the 1st week)</th>
                        <th>One week (in the 2nd week)</th>
                        <th>Two weeks</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        @if(Auth::user()->email == 'simon.goh@rajahtann.com'
                        || Auth::user()->email == 'wendy.lee@rajahtann.com'
                        || Auth::user()->email == 'ba.sou.ong@rajahtann.com')
                        <th></th>
                        @endif
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td class="user-name"><a data-name="{{$user->name}}" data-email="{{$user->email}}" data-dept="{{$user->dept}}" class="lawyer-name" style="color:#000" href="javascript:void(0)">{{$user->name}}</a></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == 'uponeday' ? 'available-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == 'dayweek' ? 'available-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == '1stweek' ? 'available-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == '2ndweek' ? 'available-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == '2week' ? 'available-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == 'notwoweek' ? 'busy-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == 'onleave' ? 'busy-checked' : null}}"></td>
                            <td class="{{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == 'other' ? 'busy-checked' : null}}">
                                {{$user->workCapacities[0] && $user->workCapacities[0]->available_capacity == 'other' ? $user->workCapacities[0]->reason_available_capacity : null}}
                            </td>
                            @if(Auth::user()->email == 'simon.goh@rajahtann.com'
                            || Auth::user()->email == 'wendy.lee@rajahtann.com'
                            || Auth::user()->email == 'ba.sou.ong@rajahtann.com')
                            <td><a class="edit-lawyer" data-name="{{$user->name}}" data-userId="{{$user->id}}" href="javascript:void(0)" style="font-size:14px; color:#000">
                                    <i class="fa fa-pencil"></i> Edit
                            </a></td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="assignment" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">
                        WCMS Work Assignment
                    </h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Subject</label>
                            <input readonly type="text" class="form-control" id="exampleInputEmail1"
                                   name="subject" value="WCMS Work Assignment" placeholder="Enter Subject">
                        </div>
                        <div class="form-group">
                            <label  for="exampleInputPassword1">Content</label>
                            <textarea readonly class="form-control" name="content">Please see me to discuss your assistance with one of my matters.</textarea>
                        </div>
                        <input style="width: 100px; background: #de6737;border-color: #de6737;" type="submit" value="Send" class="btn btn-primary"/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="lawyer-form" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 800px">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="lawyer-modal-title">
                        WCMS Work Assignment
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="padding: 0;border:none">
                        <ul style="list-style: none;padding:0;" id="error-model"></ul>
                    </div>
                    <div style="display: none" class="alert alert-success">
                        Successful updated.
                    </div>
                    <form class="check-up" method="post" action="">
                        {{csrf_field()}}
                        <input type="hidden" name="start_date" value="{{$startDate->format('Y-m-d')}}" />
                        <input type="hidden" name="end_date" value="{{$endDate->format('Y-m-d')}}" />
                        <div class="row">
                            <div class="col-lg-3">
                                <label class="items" for="uponeday">
                                    <input type="radio" name="avaible_capacity" id="uponeday" value="uponeday"/>
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/uponeday.png')}}">
                                    <span>Available for up to one day</span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="items" for="dayweek">
                                    <input type="radio" name="avaible_capacity" id="dayweek" value="dayweek" />
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/day-week.png')}}">
                                    <span>Available for more than one day, less than a week</span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="items" for="1stweek">
                                    <input type="radio" name="avaible_capacity" id="1stweek" value="1stweek"/>
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/day-week.png')}}">
                                    <span>Available for one week (in the 1st week)</span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="items" for="2ndweek">
                                    <input type="radio" name="avaible_capacity" id="2ndweek" value="2ndweek" />
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/day-week.png')}}">
                                    <span>Available for one week (in the 2nd week)</span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="items" for="2week">
                                    <input type="radio" name="avaible_capacity" id="2week" value="2week"/>
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/2week.png')}}">
                                    <span>Available for two week </span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="items" for="notwoweek">
                                    <input type="radio" name="avaible_capacity" id="notwoweek" value="notwoweek"/>
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/notwoweek.png')}}">
                                    <span>No available capacity for two weeks</span>
                                </label>
                            </div>
                            <div class="col-lg-3">
                                <label class="items" for="onleave">
                                    <input type="radio" name="avaible_capacity" id="onleave" value="onleave"/>
                                    <div class="dra-radio"></div>
                                    <img width="100" src="{{asset('images/leave.png')}}">
                                    <span>On leave / Out-of-office</span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="other-reason">
                                    <div class="box-radio">
                                        <input {{ old('avaible_capacity') == 'other' ? 'checked' : null }} type="radio" name="avaible_capacity" id="other" value="other"/>
                                        <div class="dra-radio"></div>
                                    </div>
                                    <div>If others, please specify your reason(s) below</div>
                                </label>
                                <textarea name="other_reason">{{old('other_reason')}}</textarea>

                            </div>
                        </div>
                        <input style="width: 100px; background: #de6737;border-color: #de6737;" type="submit" value="Save" class="btn btn-primary"/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_scripts')
   <script>
       var currentLawyerEmail = '';
       var currentLawyerDept = '';
       var currentLawyerId = '';
       $('.lawyer-name').click(function(){
           currentLawyerEmail = $(this).data('email');
           currentLawyerDept = $(this).data('dept');
           $('#exampleModalLabel').text('Lawyer Name: ' + $(this).data('name'));
           $('#assignment').modal('show')
       })
       $('#assignment form').submit(function(e){
           $('.ajax-loading').show()
           e.preventDefault();
           var data = $(this).serialize();
           data+= '&email='+currentLawyerEmail+'&dept='+currentLawyerDept;
           var url = '{{route('admin.send.assignment')}}'
           $.post(url, data,function(res){
               $('.ajax-loading').hide()
               $('#assignment').modal('hide')
               alert('Successful sent!');
           }).fail(function(){
               $('.ajax-loading').hide()
               alert('Error! Please try again.')
           })
       })
       $('.edit-lawyer').click(function(){
           currentLawyerId = $(this).data('userid');
           $('#lawyer-modal-title').text('Lawyer Name: ' + $(this).data('name'));
           $('#lawyer-form form')[0].reset();
           $('#lawyer-form').modal('show')
       })
       $('#lawyer-form form').submit(function(e){
           $('.ajax-loading').show()
           e.preventDefault();
           var data = $(this).serialize();
           data+= '&user_id='+currentLawyerId;
           var url = '{{route('admin.update.capacity')}}'
           $.post(url, data,function(res){
               $('.ajax-loading').hide()
               $('#lawyer-form').modal('hide')
               alert('Successful updated!');
               window.location.reload();
           }).fail(function(data){
               $('.ajax-loading').hide()
               $('#error-model').empty();
               var errors = $.parseJSON(data.responseText);
               $.each(errors, function (index, value) {
                   $('#error-model').append('<li style="padding:10px">' + value + '</li>');
               });
           })
       })
   </script>
@endsection