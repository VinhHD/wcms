@extends('layout.base')

@section('title')
    @parent
    - {{$page_title}}
@endsection

@section('page_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('css/rowGroup.dataTables.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/left_sidebar.css')}}"/>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                @include('layout.left_side')
            </div>

            <div class="col-md-9">
                <h4 class="page-title">{{$page_title}}</h4>

                <hr class="divider"/>

                <table id="list-requests" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Direct Number</th>
                        <th>Fax Number</th>
                        <th>Mobile Phone Number</th>
                        <th>Colour</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('page_scripts')
    <script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.rowGroup.min.js')}}"></script>
    <script type="text/javascript">
        var RequestsTable = function() {
            this.requests_table = $('#list-requests');

            this.init();
            this.addListener();
        };

        RequestsTable.prototype = {
            init : function() {
                var self = this;
                this.requests_table = this.requests_table.DataTable({
                    pageLength: 25,
                    searching: false,
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    dom: "<'row'<'col-sm-9'B><'col-sm-3'l>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    ajax: {
                        url: '{{route('ajax_admin_export')}}',
                        type: "POST"
                    },
                    columns: [
                        {
                            data: 'date',
                            name: 'date',
                            render: function ( data, type, row ) {
                                return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY");
                            }
                        },
                        {
                            data: 'name_on_card',
                            name: 'name_on_card'
                        },
                        {
                            data: 'designation',
                            name: 'designation'
                        },
                        {
                            data: 'direct_number',
                            name: 'direct_number'
                        },
                        {
                            data: 'fax_number',
                            name: 'fax_number'
                        },
                        {
                            data: 'mobile_number',
                            name: 'mobile_number'
                        },
                        {
                            data: 'colour',
                            name: 'colour'
                        },
                        {
                            data: 'status',
                            name: 'status',
                            render: function ( data, type, row ) {
                                var label = '';

                                switch (data) {
                                    case 'awaiting_approval':
                                        label = 'Awaiting Approval';
                                        break;
                                    case 'approved':
                                        label = 'In Progress';
                                        break;
                                    case 'ready_collection':
                                        label = 'Ready for Collection';
                                        break;
                                    default:
                                        label = data.charAt(0).toUpperCase() + data.slice(1);
                                        break;
                                }

                                return label;
                            }
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ],
                    rowGroup: {
                        dataSrc: 'date',
                        startRender: function ( rows, group ) {
                            return $('<tr/>')
                                .append('<td colspan="9"> ' + moment(group, "YYYY-MM-DD").format("DD/MM/YYYY") + '</td>');
                        }
                    }
                });
            },

            addListener: function () {

            }
        };

        $(document).ready( function () {
            new RequestsTable();
        });

    </script>
@endsection