<div id="left-sidebar">
    <h4 style="margin-top: 0">{{env('APP_NAME', 'Business Card Requisition')}}</h4>
    <ul id="treeData">
        {{--  <li><a {!!(Route::is('my_requisitions') ? 'class="active"' : '')!!} href="{{route('my_requisitions')}}"><i class="fa fa-table"></i>My Requisitions</a></li>  --}}

        @if (isAdmin())
            <li><a {!!(Route::is('admin_approval') ? 'class="active"' : '')!!} href="{{route('admin_approval')}}"><i class="fa fa-table"></i>Admin Approval</a></li>
            <li><a {!!(Route::is('admin_denied') ? 'class="active"' : '')!!} href="{{route('admin_denied')}}"><i class="fa fa-table"></i>Admin Denied</a></li>
            <li><a {!!(Route::is('admin_approved') ? 'class="active"' : '')!!} href="{{route('admin_approved')}}"><i class="fa fa-table"></i>In Progress</a></li>
            <li><a {!!(Route::is('admin_export') ? 'class="active"' : '')!!} href="{{route('admin_export')}}"><i class="fa fa-table"></i>Ready for Collection</a></li>
{{--            <li><a {!!(Route::is('admin_exported') ? 'class="active"' : '')!!} href="{{route('admin_exported')}}"><i class="fa fa-table"></i>Admin Exported</a></li>--}}
        @endif
    </ul>
</div>