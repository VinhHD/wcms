@include('layout.header')

<div class="main-content">
    @yield('content')
</div>

@include('layout.footer')

