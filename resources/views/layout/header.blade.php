<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @section('title')
            {{env("APP_NAME", "Rajah & Tann")}}
        @show
    </title>

    <!--common style-->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
    <!--common style-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!--page style-->
    @yield('page_styles')
    <!--page style-->

</head>
<body>
<div class="loader">
    <div class="spinner"></div>
</div>
<div class="container-fluid">
    <div class="row header-page">
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="{{url('/')}}"><img width="250" src="{{asset('images/rt-logo-white.png')}}"></a>
                        </div>

                        @if (Auth::check())
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a style="cursor: default;" href="javascript:void(0);">
                                        Welcome <strong>{{user('name')}}</strong>!
                                    </a>
                                </li>
                                <li class="hidden-xs">
                                    <a style="cursor: default;" href="javascript:void(0);">|</a>
                                </li>
                                {{--  <li><a href="{{route('my_requisitions')}}">My Requisitions</a></li>  --}}
                                <li>
                                    <a href="{{route('logout')}}">
                                        Logout
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                        @endif
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</div>