<footer>
    {{--<p>© 2017, Rajah & Tann. All rights are reserved</p>--}}
</footer>
<div class="ajax-loading">
    <div class="content">
        <i class="fa fa-spinner fa-spin" ></i>
    </div>
</div>
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
@yield('page_scripts')
<script type="text/javascript" src="{{asset('js/scripts.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datetimepicker1').datetimepicker({
            format: 'Y-MM-DD',
            useCurrent:false,
            daysOfWeekDisabled:[0,2,3,4,5,6]
        });
    })
</script>
</body>
</html>