@extends('layout.base')

@section('content')
    <div class="container">
        <h1 style="color: #01aff0">Welcome to {{env('APP_NAME')}} System</h1>

        <p>Please <a style="color: #01aff0" href="{{url('/')}}"><b>login</b></a> to continue.</p>
    </div>
@endsection