<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        thead > tr.title > th {
            text-align: center;
            font-weight: bold;
        }

        thead > tr:not(.title)  > th {
            border: 1px solid #000000;
            border-bottom: 1px double #000000;
            text-align: left;
            font-weight: bold;
        }
    </style>
</head>
<body>
<table width='100%' cellpadding='5' cellspacing="0" style='border-collapse: collapse;font-size:11px'>
    <thead>
    <tr class="title">
        <th colspan="8">{{$title}}</th>
    </tr>
    <tr></tr>
    <tr>
        <th>Date</th>
        <th>Name</th>
        <th>Designation</th>
        <th>Direct Number</th>
        <th>Fax Number</th>
        <th>Mobile Phone Number</th>
        <th>Colour</th>
        <th>Remark</th>
    </tr>
    </thead>
    <tbody>
        @foreach($requests as $request)
            <tr>
                <td width="15">{{dateFormat($request->date)}}</td>
                <td width="15">{{$request->user->name}}</td>
                <td width="15">{{$request->user->designation}}</td>
                <td width="15">{{$request->direct_number}}</td>
                <td width="15">{{$request->fax_number}}</td>
                <td width="15">{{$request->mobile_number}}</td>
                <td width="15">{{$request->colour}}</td>
                <td width="20">{{$request->remark}}</td>
            </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>

