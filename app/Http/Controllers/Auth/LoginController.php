<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use VinhHoang\OAuth2\Client\Provider\Azure;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $access_token;

    private $provider;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->provider = new Azure();
    }

    public function login()
    {
        $this->provider->setResource('https://graph.microsoft.com');

        if (!isset($_GET['code'])) {
            return  $this->provider->redirect();
        } else {
            $this->access_token =  $this->provider->getAccessToken('authorization_code', [
                'code' => $_GET['code'],
                'resource' => 'https://graph.microsoft.com',
            ]);

            if (request()->session()->has('redirect_url')) {
                $redirect_url = request()->session()->pull('redirect_url');
                if ($redirect_url == route('import_users')) {
                    $redirect_url .= '?code=' . $_GET['code'];
                    return redirect($redirect_url);
                }
            }
            $resourceOwner = $this->provider->getResourceOwner($this->access_token);
            Auth::login($this->updateOrCreateUser($resourceOwner), true);
            if(Auth::user()->cluster_head == 1){
                return redirect(route('admin.checkUp'));
            }else{
                return redirect(route('workflow.checkUp'));
            }

        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $redirect_url = route('welcome');
        return redirect($this->provider->getLogoutUrl($redirect_url));
    }

    public function updateOrCreateUser($resourceOwner)
    {
        $detailUser = $this->provider->get('me/?$select=department,jobTitle,displayName,businessPhones,officeLocation,userPrincipalName', $this->access_token);

        if (filter_var($resourceOwner->getEmail(),FILTER_VALIDATE_EMAIL)) {
            $email = $resourceOwner->getEmail();
        } else if (filter_var($resourceOwner->getUpn(), FILTER_SANITIZE_EMAIL)) {
            $email = $resourceOwner->getUpn();
        } else {
            $email = $detailUser['userPrincipalName'];
        }

        $user = User::where('email', "$email")->orWhere('oid', $resourceOwner->getId())->first();

        if ($user) {
            $user->oid = $resourceOwner->getId();
            $user->office = $detailUser['officeLocation'];
            $user->save();
        } else {
            throw new \Exception("Your email does not exist on the system");
        }

        return $user;
    }
}
