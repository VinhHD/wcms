<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Repositories\OdbcRepository;

class UsersController extends Controller
{


    public function import_user(OdbcRepository $odbc)
    {
        //$raw_users = $this->odbc->results("SELECT * FROM [CentralLookup].[dbo].[v_HBM_PERSNL_Email] WHERE withAD='Y' ORDER BY employee_code ASC OFFSET 0 ROWS FETCH NEXT 100 ROWS ONLY");
        $raw_users = $odbc->results("SELECT * FROM [CentralLookup].[dbo].[v_HBM_PERSNL_Email] hp
                                        LEFT JOIN [V_Personnel] pe ON hp.email = pe.Email
                                        WHERE hp.withAD='Y' AND hp.PERSNL_TYP_CODE <> 'INACT' ORDER BY hp.employee_code ASC");

        // $n = 0;
        $n_updated = 0;
        $n_created = 0;

        $import_email = array();
        //do {
        //starting importing
        foreach ($raw_users as $u) {
            if ($u['email'] && filter_var($u['email'], FILTER_VALIDATE_EMAIL)) {
                $user = User::where('email', $u['email'])->first();

                $import_email[] = $u['email'];

                if (!$user) {
                    $user = new User();
                    $n_created++;
                } else {
                    $n_updated++;
                }

                $user->email = $u['email'];
                $user->name = str_replace("\xB4", " ", $u['femployee_name']);
                $user->note_name = str_replace("\xB4", " ", $u['EMPLOYEE_NAME']);
                $user->department = $u['PDepartment'];
                $user->designation = $u['PDesignation'];
                $user->employee_code = $u['employee_code'];
                if ($u['PExtn']) {
                    $user->extn = $u['PExtn'];
                }
                if ($u['OFFC']) {
                    $user->office_country = $u['OFFC'];
                }
                if ($u['OfficeFAXPhoneNumber']) {
                    $user->fax_number = $u['OfficeFAXPhoneNumber'];
                }
                if ($u['AdmissionDate']) {
                    $user->admission_date = dateFormat($u['AdmissionDate'], ['d/m/Y', 'd m Y'], 'Y-m-d');
                }
                if ($u['AdmissionNo'] && $u['AdmissionNo'] != '-') {
                    $user->admission_no = $u['AdmissionNo'];
                }
                if ($u['Cluster']) {
                    $user->cluster = $u['Cluster'];
                }
                $user->save();

                if ($user->trashed()) {
                    $user->restore();
                }
            }
        }

        //$offset = ($n + 1) * 100;
        //$take = 100;

        //$raw_users = $this->odbc->results("SELECT * FROM [CentralLookup].[dbo].[v_HBM_PERSNL_Email] ORDER BY employee_code ASC OFFSET $offset ROWS FETCH NEXT $take ROWS ONLY");
        //$n++;

        // } while (sizeof($raw_users) > 0);

        $odbc->close_conn();
        //end importing

        //sync db
        $current_email = User::where('email', '<>', 'quapetest@rajahtann.com')->select('email')->get()->toArray();
        $old_email = array();

        foreach ($current_email as $item) {
            $old_email[] = $item['email'];
        }

        $delete_email = array_diff($old_email, $import_email);
        User::whereIn('email', $delete_email)->delete();
        //sync db

        if (!empty($_GET['redirect'])) {
            return back();
        }

        echo "Number of new user: " . $n_created . "<br/>";
        echo "Number of updated user: " . $n_updated . "<br/>";

        die();
    }
}