<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

class FilesController extends Controller
{
    public function download()
    {
        $file = session()->pull('file-download');

        $now = Carbon::now()->format('d-m-Y');
        $filename = 'Under File ' . $now;

        return response()->download($file, $filename . '.xls')->deleteFileAfterSend(true);
    }
}