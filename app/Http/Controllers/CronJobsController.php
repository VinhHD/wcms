<?php
namespace App\Http\Controllers;
use App\Models\CmsSetting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\Reminder;
use Illuminate\Support\Facades\Log;
class CronJobsController extends Controller
{
    public function reminder(){
        Log::info('Starting cronjob');
        $lastSending = CmsSetting::where('name', 'last_sending_reminder')->first()->content;
        $nextSending = Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(14);
        $users = User::where('cluster_head', null)->orWhere('cluster_head', 0)->get();
        echo 'Last Sending: ' .$lastSending."</br>";
        echo 'Next Sending: ' .$nextSending->format('Y-m-d')."</br>";
        if($nextSending->format('Y-m-d') == date('Y-m-d')){
            Log::info('Sending reminder');
            foreach($users as $user){
                try {
                    Mail::to($user->email)->send(new Reminder($user));
                    Log::info('Sent '. $user->email);
                }catch (\Exception $e){
                    Log::info($e->getMessage().$user->email);
                }
            }
            Log::info('Updating setting');
            try{
                
                CmsSetting::where('name', 'last_sending_reminder')->update([
                    'content' => date('Y-m-d')
                ]);
            }catch (\Exception $e){
                Log::info($e->getMessage());
            }


        }
    }
}