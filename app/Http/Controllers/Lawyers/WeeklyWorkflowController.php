<?php

namespace App\Http\Controllers\Lawyers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\WorkCapacity;
use App\Models\CmsSetting;
class WeeklyWorkflowController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkUp(){
        $page_title = 'Fortnightly Workflow';
        return view('lawyers.check-up', compact('page_title'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $selected = $request->input('avaible_capacity');
        $rules = [
            'avaible_capacity' => 'required',
        ];
        $messages = [
            'avaible_capacity.required' => 'Please select one option.',
        ];
        if($selected == 'other'){
            $rules = [
                'other_reason' => 'required',
            ];
            $messages = [
                'other_reason.required' => 'Please enter the reason.',
            ];
        }
        $this->validate($request, $rules, $messages);
        $this->saveWorkCapacity($selected, $request->input('other_reason'));
        return redirect(route('workflow.checkUp'))->with('status', 'updated');
    }

    /**
     * @param $available
     * @param $other
     * @return string
     */
    public function saveWorkCapacity($available, $other){
        $userId = Auth::id();
        $lastSending = CmsSetting::where('name', 'last_sending_reminder')->first()->content;
        $getMondayLastSending =  Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(3);
        $end = Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(14);
        $startDate = $getMondayLastSending->format('Y-m-d');
        $endDate = $end->format('Y-m-d');
//        if($lastSending == date('Y-m-d')){
//           if(date('H') < 18){
//               $startDate = Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(-11)->format('Y-m-d');
//               $endDate = Carbon::createFromFormat('Y-m-d',$lastSending)->format('Y-m-d');
//           }
//        }
        try{
            DB::beginTransaction();
            $workCapacity = WorkCapacity::where('user_id', $userId)
            ->where('startDate', $startDate)
            ->where('endDate', $endDate)->first();
            if($workCapacity->id){
                WorkCapacity::where('id', $workCapacity->id )->update([
                    'available_capacity' => $available,
                    'reason_available_capacity' => $other,
                ]);
            }else{
                $workCapacity = new WorkCapacity();
                $workCapacity->user_id = $userId;
                $workCapacity->startDate = $startDate;
                $workCapacity->endDate = $endDate;
                $workCapacity->available_capacity = $available;
                $workCapacity->reason_available_capacity = $other;
                $workCapacity->save();
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }


    
}