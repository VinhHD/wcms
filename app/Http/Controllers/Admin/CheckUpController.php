<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\WorkAssignment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\WorkCapacity;
use App\Repositories\OdbcRepository;
use App\Models\CmsSetting;
class CheckUpController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){
        $startDateRequest = $request->get('start_date');
        $lastSending = CmsSetting::where('name', 'last_sending_reminder')->first()->content;
        $getMondayLastSending =  Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(3);
        $end = Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(14);
        $startDate = $getMondayLastSending;
        $endDate = $end;
        if($startDateRequest){
            $startDate = Carbon::createFromFormat('Y-m-d',$startDateRequest);
            $endDate = Carbon::createFromFormat('Y-m-d',$startDateRequest)->addDays(11);
        }else if(strtotime($getMondayLastSending->format('Y-m-d')) > strtotime(date('Y-m-d'))){
            // if today less than next monday of last sending then get old monday of week
            $startDate = Carbon::createFromFormat('Y-m-d',$lastSending)->addDays(-11);
            $endDate = Carbon::createFromFormat('Y-m-d',$lastSending);
        }
        $schedule = $startDate->format('d F Y') . ' to ' . $endDate->format('d F Y');
        $users = User::with(['workCapacities' => function($query) use ($startDate, $endDate){
            $query->where('startDate', $startDate->format('Y-m-d'))
            ->where('endDate', $endDate->format('Y-m-d'));
        }])->where('cluster_head', null)
        ->orWhere('cluster_head', 0)->get();
        $page_title = 'Fortnightly Workflow';
        return view('admin.checkup', compact('page_title', 'users', 'schedule','startDate', 'endDate'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function postWorkAssignment(Request $request, OdbcRepository $odbc){
        try{
            $emails = [];
            $pgHeads = $odbc->results("SELECT * FROM V_PGHead WHERE Dept = ".$request->input('dept'));
            if($pgHeads){
                foreach($pgHeads as $head){
                    $users = $odbc->results("SELECT * FROM [CentralLookup].[dbo].[v_HBM_PERSNL_Email] hp
                       LEFT JOIN [V_Personnel] pe ON hp.email = pe.Email
                       WHERE hp.withAD='Y' AND hp.PERSNL_TYP_CODE <> 'INACT'
                       AND hp.employee_code = '".$head['Employee_Code']."'
                       ORDER BY hp.employee_code ASC");
                    if($users){
                        foreach($users as $user){
                            $emails[] = $user['email'];
                        }
                    }
                }
            }
            if(!in_array('simon.goh@rajahtann.com',$emails)){
                $emails[] = 'simon.goh@rajahtann.com';
            }
            Mail::to($request->input('email'))->cc($emails)->send(new WorkAssignment());
        }catch (\Exception $e){
            return response($e->getMessage(),422);
        }
        return response('success');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $selected = $request->input('avaible_capacity');
        $rules = [
            'avaible_capacity' => 'required',
        ];
        $messages = [
            'avaible_capacity.required' => 'Please select one option.',
        ];
        if($selected == 'other'){
            $rules = [
                'other_reason' => 'required',
            ];
            $messages = [
                'other_reason.required' => 'Please enter the reason.',
            ];
        }
        $startDate = $request->input('start_date');
        $endDate = $request->input('end_date');
        $this->validate($request, $rules, $messages);
        $this->saveWorkCapacity($selected,
        $request->input('other_reason'),
        $request->input('user_id'),
        $startDate, $endDate);
//        return redirect(route('workflow.checkUp'))->with('status', 'updated');
    }

    /**
     * @param $available
     * @param $other
     * @return string
     */
    public function saveWorkCapacity($available, $other, $userId, $startDate, $endDate){
        try{
            DB::beginTransaction();
            $workCapacity = WorkCapacity::where('user_id', $userId)
                ->where('startDate', $startDate)
                ->where('endDate', $endDate)->first();
            if($workCapacity->id){
                WorkCapacity::where('id', $workCapacity->id )->update([
                    'available_capacity' => $available,
                    'reason_available_capacity' => $other,
                ]);
            }else{
                $workCapacity = new WorkCapacity();
                $workCapacity->user_id = $userId;
                $workCapacity->startDate = $startDate;
                $workCapacity->endDate = $endDate;
                $workCapacity->available_capacity = $available;
                $workCapacity->reason_available_capacity = $other;
                $workCapacity->save();
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }
}