<?php

namespace App\Repositories;

class OdbcRepository
{
    private $_connection;
    private $_username = "lookupadm";
    private $_password = "lookup234";
    private $_database = "CentralLookup";

    // Constructor
    public function __construct()
    {
        $conn = odbc_connect($this->_database, $this->_username, $this->_password);

        // Error handling
        if ($conn == false) {
            trigger_error("Failed to conencto to SQL server: " . odbc_errormsg($conn), E_USER_ERROR);
        } else {
            $this->_connection = $conn;
        }
    }

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance()
    {
        if (!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    // Magic method clone is empty to prevent duplication of connection
    private function __clone()
    {
    }

    // Get mysqli connection
    public function get_conn()
    {
        return $this->_connection;
    }

    public function close_conn()
    {
        return odbc_close($this->_connection);
    }

    public function exec_query($sql)
    {
        if (!empty($sql)) {
            $query = odbc_exec($this->_connection, $sql) or die("DATABASE QUERY ERROR " . odbc_errormsg());
            return $query;
        } else {
            $this->close_conn();
            return false;
        }
    }

    public function num_rows($query)
    {
        $num = odbc_num_rows($query);
        if ($num > 0) {
            return $num;
        } else {
            return false;
        }
    }

    public function num_fields($query)
    {
        $fields = odbc_num_fields($query);
        if ($fields > 0) {
            return $fields;
        } else {
            return false;
        }
    }

    public function go_next($query)
    {
        if (is_resource($query)) {
            $next = odbc_next_result($query);
        }

        return $next;
    }

    public function results($sql)
    {

        $count = 0;
        $data = array();

        $res = $this->exec_query($sql);

        while ($row = @odbc_fetch_array($res)) {
            $data[$count] = $row;
            $count++;
        }
        @odbc_free_result($res);
        return $data;

    }
}