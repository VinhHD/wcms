<?php

namespace App\Repositories;

use App\Models\Settings;

class SettingsRepository
{
    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function getSetting($key)
    {
        $value = $this->settings->where('key', $key)->first();

        return $value ? $value->value : null;
    }

    public function create(array $data)
    {
        return $this->settings->create($data);
    }

    public function update(array $data)
    {
        return $this->settings->update($data);
    }

}