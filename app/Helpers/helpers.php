<?php

use Carbon\Carbon;
use App\Models\Settings;
function clusterHeadColumn($data){
    if($data == 1){
        return "Yes";
    }
    return "No";
}
if (!function_exists('user')) {
    function user($key = null)
    {
        static $user = null;

        if ($user === null) {
            $user = auth()->user();
        }

        if ($key === null) {
            return $user;
        }

        if (isset($user->{$key})) {
            return $user->{$key};
        }

        return null;
    }
}

if (!function_exists('dateFormat')) {
    function dateFormat($date, $input_format = 'Y-m-d', $output_format = 'd/m/Y')
    {
        $format_date = null;

        if (!$date) {
            return null;
        }

        if (gettype($input_format) == 'string') {
            $format_date =  Carbon::createFromFormat($input_format, $date)->format($output_format);
        }

        if (gettype($input_format) == 'array') {
            foreach ($input_format as $format) {
                try {
                    $format_date = Carbon::createFromFormat($format, $date)->format($output_format);
                    break;
                } catch (\Exception $e) {
                    continue;
                }
            }
        }

        return $format_date;
    }
}

if (!function_exists('currentTime')) {
    function currentTime($output_format = 'd/m/Y')
    {
        return Carbon::now()->format($output_format);
    }
}

if (!function_exists('statusLabel')) {

    function statusLabel($status = null)
    {
        $label = '';
        switch ($status) {
            case 'awaiting_approval':
                $label = 'Awaiting Approval';
                break;
            case 'approved':
                $label = 'In Progress';
                break;
            case 'ready_collection':
                $label = 'Ready for Collection';
                break;
            default:
                $label = ucfirst($status);
                break;
        }

        return $label;
    }
}

function getSettings($key)
{
    $setting = Settings::where('key', $key)->first();

    return ($setting) ? $setting->value : '';
}

function isAdmin()
{
   return user('cluster_head') == 1;
}