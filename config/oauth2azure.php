<?php
return [
    'clientId'          => env('AZURE_CLIENT_ID'),
    'clientSecret'      => env('AZURE_CLIENT_SECRET'),
    'redirectUri'       => env('AZURE_REDIRECT'),
    'tenant'            => env('AZURE_TENANT'),
];